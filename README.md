# Capture pi scripts and setup

You should familiarize yourself with how HypriotOS works first: 

* https://blog.hypriot.com/faq/

* https://blog.hypriot.com/getting-started-with-docker-on-your-arm-device/

The basics of how this setup works is that the cloud-init setup does some setup steps (mainly to do with getting and keeping the correct time) and then runs a docker container that takes photos using the raspbery pi camera. This docker container is setup to run in such a way that it uses the **hostname** to name the resulting files.

The following steps are what I use to setup a Raspberrry Pi:

1. download and unzip HypriotOS: 
    ```bash
    wget 'https://github.com/hypriot/image-builder-rpi/releases/download/v1.12.2/hypriotos-rpi-v1.12.2.img.zip'
    unzip 'hypriotos-rpi-v1.12.2.img.zip'
    ```
2. read through the *capture-zero.yaml* user-data file and change values that need changing. Pay attention to the users mapping, and wifi, because that will allow you to log into the pi once it has started up so you can check to make sure it is working.

3. run the flash script with the appropriate parameters. Replace `<hostname>`, `<dev>`, and `<rtc>` values with values appropriate for your system.
    ```bash
    sudo ./flash -F docker-compose.yml -n '<hostname>' -u capture-zero.yaml --bootconf 'config-<rtc>.txt' -d /dev/<dev> hypriotos-rpi-v1.12.0.img
    ```
4. make sure your computer has finished writing to the sd card and that it is not mounted. The flash script should take care of this, but I run `sync` just to be sure. Run `lsblk` to make sure that the sd card is no longer mounted.

5. unplug the sd card from your sd card reader/writer and plug it into the raspberry pi that you will use. _It is important that use the same device that you intend to use to capture from here! I have wasted a lot of time trying to work out some silly device naming mystery or another. Its not worth it: **just use the same device.**_

6. checklist:
    * RTC connected & has a battery installed.
    * wifi network in range and setup correctly, DHCP, internet access, client to client access from your pc (so you can log in and check functionality).

7. apply power to the raspberry pi. If you are using a raspberry pi zero, it will likely take a very long time to boot and start provisioning. 

8. work out the ip address of the raspberry pi and ssh in from your pc using the details you filled out in the *capture-zero.yaml* file.

9. once you have logged in you can check the status of provisioning with the command `sudo journalctl -r` and the contents of `/data`.


## Notes

### Images

The images should be written in a structured directory format to the /data directory of the 'root' partition of the sd card. As this partition is formatted with ext4, you will need a linux computer to be able to mount this partition and retrieve the data.

If you were to have the pi connected to the internet permanently, you could use an sftp docker container or my [b2-uploader](https://gitlab.com/gdunstone/b2-uploader) docker container to upload images to the cloud for on-the-fly processing & analysis.


### RTCs

I've included bootconfs for the 2x rtcs that we use most commonly, an ebay ds3231 and a RasClock from AfterThoughtSoftware (which uses either the NXP PCF2127T/AT or the NXP PCF2129T/AT. 

Both clocks use the I²C protocol and use pins 3 & 5 (I²C SDA & I²C SCL on the rpi). The ds3132 that we got uses pin 1 for vcc and pin 9 for ground

### flash script

The flash script that is included here has some additions on top of what is in the normal flash script from HypriotOS, but for most use cases it isnt important. The additions are helpers for  managing always connected devices and their wireguard vpn profiles, this setup uses the standard hypriotos image, not the custom built one that includes wireguard.

